import Vue from 'vue';
import Router from 'vue-router';
import App from './App.vue';
import store from './store/store';
import router from './router';
import Favorite from './components/Favorite.vue';

Vue.use(Router);
Vue.config.productionTip = false
Vue.component('favorite', Favorite);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'app',
      component: App 
    },
    {
      path: '/favorites',
      name: 'favorites',
      component: Favorite
    }
  ]
});

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
})
