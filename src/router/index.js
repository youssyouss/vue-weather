import Vue from 'vue'; 
import Router from 'vue-router';
import Current from '../components/Favorite.vue';
import Favorites from '../components/Favorite.vue';

Vue.use(Router);

export default new Router({
    routes:[
        {
            path: '/',
            name:'Current',
            component: Current
        },
        {
            path: '/favorites',
            name: 'Favorites',
            component: Favorites
        }
    ]
})