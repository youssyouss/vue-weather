

const state = {
    favorites : [], 
    selected: '',
}; 

const getters = {
    allFavorites: state => state.favorites,
    selected: state => state.selected
};

const actions = {
    // fetchFav from localStorage

    addFav ({commit}, favorite) 
    {
        commit('newFav', favorite);
    }, 

    deleteFav({commit}, favorite)
    {
        commit('deleteFav', favorite);
    },
    selectedFav({commit}, favorite){
        commit('selectedFav', favorite);
    }
}; 

const mutations = {
    newFav: (state, favorite) => state.favorites.push(favorite),
    deleteFav: (state, favorite) => {
        let index = state.favorites.indexOf(favorite);
        state.favorites.splice(index, 1);
    },
    selectedFav: (state, favorite) => {
        state.selected = favorite;
    }
}; 

export default {
    state,
    getters, 
    actions, 
    mutations
}